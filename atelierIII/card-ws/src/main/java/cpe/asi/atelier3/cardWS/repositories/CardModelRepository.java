package cpe.asi.atelier3.cardWS.repositories;

import cpe.asi.atelier3.cardWS.model.CardModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CardModelRepository extends CrudRepository<CardModel, UUID> {

}
