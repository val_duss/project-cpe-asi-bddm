#!/bin/bash
echo ""
echo "      --- BUILDING SPRING APPS ---"
echo ""
mvn clean install

echo ""
echo "      --- LAUNCHING CONTAINERS ---"
echo ""
docker-compose up -d --remove-orphans

echo ""
echo "      --- CREATING ROOMS DATABASE ---"
echo ""
./migrate-rooms-db.sh
