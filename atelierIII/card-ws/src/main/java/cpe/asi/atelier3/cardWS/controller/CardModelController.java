package cpe.asi.atelier3.cardWS.controller;

import cpe.asi.atelier3.cardWS.model.CardModel;
import cpe.asi.atelier3.cardWS.service.CardModelService;
import cpe.asi.atelier3.modelDTO.CardModelDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class CardModelController {

    @Autowired
    private CardModelService cardModelService;

    @Autowired
    private ModelMapper modelMapper;

    /**
     * Read - Get one cardmodel
     * @param id The id of the user
     * @return An CardModel object full filled
     */
    @GetMapping("/api/cardmodel/{id}")
    public CardModelDTO getById(@PathVariable final UUID id) {
        return cardModelService.getCardModel(id).map(this::convertToDto).orElse(null);
    }

    /**
     * Read - Get all cardmodels
     * @return A list of CardModel object full filled
     */
    @GetMapping("/api/cardmodel")
    public List<CardModelDTO> getAll() {
        List<CardModel> cardModelList = (List<CardModel>) cardModelService.getAllCardModel();
        return cardModelList.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    /**
     * Create - Add a new cardmodel
     * @param cardModelDTO An object user
     * @return The cardmodel object saved
     */
    @PostMapping("/api/cardmodel")
    public CardModelDTO create(@RequestBody CardModelDTO cardModelDTO) throws ParseException {
        CardModel cardModel = convertToEntity(cardModelDTO);
        CardModel cardModelCreate = cardModelService.createCardModel(cardModel);
        return convertToDto(cardModelCreate);
    }

    /**
     * Delete - Delete an cardmodel
     * @param id - The id of the cardmodel to delete
     */
    @DeleteMapping("/api/cardmodel/{id}")
    public void delete(@PathVariable final UUID id) {
        cardModelService.deleteCardModel(id);
    }

    private CardModel convertToEntity(CardModelDTO cardModelDto) throws ParseException {
        CardModel cardModel = modelMapper.map(cardModelDto, CardModel.class);
        cardModel.setPrice(cardModelDto.getPrice());
        cardModel.setName(cardModelDto.getName());
        return cardModel;
    }

    private CardModelDTO convertToDto(CardModel cardModel) {
        CardModelDTO cardModelDTO = modelMapper.map(cardModel, CardModelDTO.class);
        cardModelDTO.setPrice(cardModel.getPrice());
        cardModelDTO.setName(cardModel.getName());
        return cardModelDTO;
    }
}
