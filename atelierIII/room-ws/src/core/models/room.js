class Room {
    id;
    user_1_id;
    user_2_id;

    constructor(id, user_1_id, user_2_id) {
        this.id = id;
        this.user_1_id = user_1_id;
        this.user_2_id = user_2_id;
    }

    /**
     *
     * @param {*} json
     * @param {string} caserne
     * @returns Objet Room à partir des données json
     */
    static fromJSON(json) {
        return new Room(
            json["id"],
            json["user_1_id"],
            json["user_2_id"],
        );
    }
}

module.exports = { Room };
