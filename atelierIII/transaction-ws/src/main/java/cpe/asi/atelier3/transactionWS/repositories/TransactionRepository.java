package cpe.asi.atelier3.transactionWS.repositories;

import cpe.asi.atelier3.transactionWS.models.Transaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, UUID> {

}
