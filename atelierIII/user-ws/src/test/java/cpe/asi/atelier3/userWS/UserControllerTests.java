package cpe.asi.atelier3.userWS;

import cpe.asi.atelier3.userWS.controllers.UserController;
import cpe.asi.atelier3.userWS.models.User;
import cpe.asi.atelier3.userWS.services.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@WebMvcTest(value = UserController.class)
class UserControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserService userService;

	User mockUser = new User( UUID.fromString("f5bd1239-59f5-4c42-a9cd-f6cfe8619ae4"),"user","123",0);

	String exampleUserJson = "{\n" +
			"    \"id\": \"f5bd1239-59f5-4c42-a9cd-f6cfe8619ae5\",\n" +
			"    \"name\": \"user\",\n" +
			"    \"password\": \"123\",\n" +
			"    \"balance\": 0.0\n" +
			"}";

	@Test
	public void GetUser() throws Exception {
		User user = new User( UUID.fromString("f5bd1239-59f5-4c42-a9cd-f6cfe8619ae4"),"user","123",0);
		userService.createUser(user);
		Mockito.when(userService.getUser(UUID.fromString("f5bd1239-59f5-4c42-a9cd-f6cfe8619ae4")).get()).thenReturn(mockUser);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/api/user/f5bd1239-59f5-4c42-a9cd-f6cfe8619ae4").accept(
				MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse());

		String exeptedUserJson = "{\n" +
				"    \"id\": \"f5bd1239-59f5-4c42-a9cd-f6cfe8619ae4\",\n" +
				"    \"name\": \"user\",\n" +
				"    \"password\": \"123\",\n" +
				"    \"balance\": 0.0\n" +
				"}";

		JSONAssert.assertEquals(exeptedUserJson, result.getResponse()
				.getContentAsString(), false);
	}

	@Test
	public void createUSer() throws Exception {
		User mockUser = new User( UUID.fromString("f5bd1239-59f5-4c42-a9cd-f6cfe8619ae5"),"user","123",0);

		// studentService.addCourse to respond back with mockCourse
		Mockito.when(userService.createUser(
				Mockito.any(User.class))).thenReturn(mockUser);

		// Send course as body to /students/Student1/courses
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/api/user")
				.accept(MediaType.APPLICATION_JSON).content(exampleUserJson)
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.CREATED.value(), response.getStatus());

		assertEquals("http://localhost:8082/apit/user/f5bd1239-59f5-4c42-a9cd-f6cfe8619ae5",
				response.getHeader(HttpHeaders.LOCATION));

	}

}
