const router = require("express").Router();
const swaggerUi = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');

//  setup swagger
const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Rooms Web Service',
            version: '1.0.0',
        },
    },
    apis: ['./src/services/rooms/rooms_service.js'],
};
const swaggerSpec = swaggerJSDoc(options);

// serve swagger
router.use('/swagger-ui', swaggerUi.serve);
router.get('/swagger-ui', swaggerUi.setup(swaggerSpec));

// import services here
router.use("/api/rooms", require("../../services/rooms/rooms_service"));

//  router error handler
router.use(function (err, _req, res, next) {
    if (err.name === "ValidationError") {
        return res.status(422).json({
            errors: Object.keys(err.errors).reduce(function (errors, key) {
                errors[key] = err.errors[key].message;

                return errors;
            }, {}),
        });
    }

    return next(err);
});

module.exports = router;
