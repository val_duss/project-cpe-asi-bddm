package cpe.asi.atelier3.modelDTO;

import java.util.UUID;

public class CardModelDTO {

    private UUID id;
    private String name;
    private int attack;
    private int healthPoint;
    private int defense;
    private double price;

    public CardModelDTO() {
    }

    public CardModelDTO(String name, double price) {
        this.name = name;
        this.price = price;
        this.attack = 0;
        this.healthPoint = 0;
        this.defense = 0;
    }

    public CardModelDTO(String name, int attack, int healthPoint, int sheild, double price) {
        this.name = name;
        this.attack = attack;
        this.healthPoint = healthPoint;
        this.defense = sheild;
        this.price = price;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getHealthPoint() {
        return healthPoint;
    }

    public void setHealthPoint(int healthPoint) {
        this.healthPoint = healthPoint;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int sheild) {
        this.defense = sheild;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}