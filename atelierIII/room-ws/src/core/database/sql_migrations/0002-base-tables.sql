-- schema pour les données métiers
CREATE SCHEMA IF NOT EXISTS asi;

--
-- ROOMS
--
CREATE TABLE IF NOT EXISTS asi.t_room (
    id UUID DEFAULT gen_random_uuid(),
    user_1_id UUID,
    user_2_id UUID,
    PRIMARY KEY (id)
);
