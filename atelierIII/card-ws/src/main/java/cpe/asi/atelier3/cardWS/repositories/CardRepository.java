package cpe.asi.atelier3.cardWS.repositories;

import cpe.asi.atelier3.cardWS.model.Card;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CardRepository extends CrudRepository<Card, UUID> {
    @Query("select c from Card c where c.ownerUser = ?1")
    public Iterable<Card> findByUser(UUID uuid); //TODO faire un like sur les tags


}
