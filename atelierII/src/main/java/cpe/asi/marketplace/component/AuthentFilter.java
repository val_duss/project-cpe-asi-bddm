package cpe.asi.marketplace.component;

import org.apache.catalina.LifecycleState;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Component
public class AuthentFilter extends OncePerRequestFilter {

    List<String> unsecuredUrls = Arrays.asList("user/login");

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String tokenJwt = null;
        Cookie[] cookies = request.getCookies();
        if(cookies != null){
            tokenJwt = Arrays.stream(cookies).filter(c -> "JWT".equals(c.getName())).findFirst()
                    .map(cookie -> cookie.getValue())
                    .orElse(null);
        }
        if(tokenJwt == null){
            response.setStatus(401);
        }
        filterChain.doFilter(request, response);
    }

    protected boolean shoudNotFilter(HttpServletRequest httpServletRequest) throws ServletException{
        return unsecuredUrls.contains(httpServletRequest.getServletPath());
    }
}
