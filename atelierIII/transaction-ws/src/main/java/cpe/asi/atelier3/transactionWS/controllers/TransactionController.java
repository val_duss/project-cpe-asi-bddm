package cpe.asi.atelier3.transactionWS.controllers;

import cpe.asi.atelier3.modelDTO.TransactionDTO;
import cpe.asi.atelier3.transactionWS.models.Transaction;
import cpe.asi.atelier3.transactionWS.services.TransactionService;
import cpe.asi.atelier3.modelDTO.CardDTO;
import cpe.asi.atelier3.modelDTO.UserDTO;
import cpe.asi.atelier3.utils.ClientErrorHandler;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


import java.text.ParseException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private RestTemplate restTemplate;

    /**
     * Read - Get one Transaction
     * @param id The id of the Transaction
     * @return An Transaction object full filled
     */
    @GetMapping("/api/transaction/{id}")
    public TransactionDTO getById(@PathVariable final UUID id) {
        return transactionService.getTransaction(id).map(this::convertToDto).orElse(null);
    }

    @GetMapping("/api/transaction")
    public List<TransactionDTO> getAll() {
        List<Transaction> transactionList = (List<Transaction>) transactionService.getAllTransaction();
        return transactionList.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }


    @PostMapping("/api/transaction")
    public TransactionDTO create(@RequestBody TransactionDTO transactionDTO) throws ParseException {
        Transaction transaction = convertToEntity(transactionDTO);
        Transaction transactionCreate = transactionService.createTransaction(transaction);
        return convertToDto(transactionCreate);
    }



    @DeleteMapping("/api/transaction/{id}")
    public void delete(@PathVariable final UUID id) {
        transactionService.deleteTransaction(id);
    }

    @PutMapping("/api/transaction/{id}")
    public TransactionDTO putBuyer(@RequestBody TransactionDTO transactionDTO, @PathVariable final UUID id) throws ParseException {
        Transaction transaction = convertToEntity(transactionDTO);
        TransactionDTO transactionDTOUpdated = convertToDto(transactionService.updateTransaction(id, transaction));
        return transactionDTOUpdated;
    }

    private Transaction convertToEntity(TransactionDTO transactionDTO) throws ParseException {
        Transaction transaction = modelMapper.map(transactionDTO, Transaction.class);

        if(transactionDTO.getSellerUserUUIDStr() != null || transactionDTO.getSellerUser() != null) {
            UserDTO seller = null;
            try {
                seller = restTemplate.getForObject("http://localhost:8082/api/user/" + transactionDTO.getSellerUser().getId().toString(), UserDTO.class);
            } catch (ClientErrorHandler.ResourceNotFoundException e) {
                seller = new UserDTO();
                seller.setId(UUID.fromString(transactionDTO.getSellerUserUUIDStr()));
                e.printStackTrace();
            }
            transaction.setSellerUser(seller.getId());
        }

        if(transactionDTO.getCardUUIDStr() != null || transactionDTO.getCard() != null) {
            CardDTO card;
            try {
                card = restTemplate.getForObject("http://localhost:8081/api/card/" + transactionDTO.getCardUUIDStr(), CardDTO.class);
            } catch (ClientErrorHandler.ResourceNotFoundException e) {
                card = new CardDTO();
                card.setId(UUID.fromString(transactionDTO.getCardUUIDStr()));
                e.printStackTrace();
            }
            transaction.setCard(card.getId());
        }

        if(transactionDTO.getBuyerUser() != null || transactionDTO.getBuyerUserUUIDStr() != null) {
            UserDTO buyer;
            try{
                buyer = restTemplate.getForObject("http://localhost:8082/api/user/"+transactionDTO.getBuyerUser().getId().toString(), UserDTO.class);
            } catch (ClientErrorHandler.ResourceNotFoundException e) {
                buyer = new UserDTO();
                buyer.setId(transactionDTO.getBuyerUser().getId());
                e.printStackTrace();
            }
            transaction.setBuyerUser(buyer.getId());
        }
        return transaction;
    }

    private TransactionDTO convertToDto(Transaction transaction) {
        TransactionDTO transactionDTO = modelMapper.map(transaction, TransactionDTO.class);
        if(transaction.getSellerUser() != null) {
            UserDTO seller;
            try {
                seller = restTemplate.getForObject("http://localhost:8082/api/user/" + transaction.getSellerUser(), UserDTO.class);
            } catch (Exception e) {
                seller = new UserDTO();
                seller.setId(transaction.getSellerUser());
                e.printStackTrace();
            }
            transactionDTO.setSellerUser(seller);
        }

        if(transaction.getCard() != null) {
            CardDTO card;
            try {
                card = restTemplate.getForObject("http://localhost:8081/api/card/" + transaction.getCard().toString(), CardDTO.class);
            } catch (Exception e) {
                card = new CardDTO();
                card.setId(transaction.getCard());
                e.printStackTrace();
            }
            transactionDTO.setCard(card);
        }

        if(transaction.getBuyerUser() != null) {
            UserDTO buyer;
            try{
                buyer = restTemplate.getForObject("http://localhost:8082/api/user/"+transaction.getBuyerUser().toString(), UserDTO.class);
            } catch (Exception e) {
                buyer = new UserDTO();
                buyer.setId(transaction.getBuyerUser());
                e.printStackTrace();
            }

            transactionDTO.setBuyerUser(buyer);
        }
        return transactionDTO;
    }





}