package cpe.asi.atelier3.cardWS.controller;

import cpe.asi.atelier3.cardWS.model.Card;
import cpe.asi.atelier3.cardWS.model.CardModel;
import cpe.asi.atelier3.cardWS.service.CardModelService;
import cpe.asi.atelier3.cardWS.service.CardService;
import cpe.asi.atelier3.modelDTO.CardDTO;
import cpe.asi.atelier3.modelDTO.CardModelDTO;
import cpe.asi.atelier3.modelDTO.UserDTO;
import cpe.asi.atelier3.utils.ClientErrorHandler;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class CardController {

    private final String UserAPI = "http://localhost:8082/api/user/";

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private CardService cardService;

    @Autowired
    private CardModelService cardModelService;

    @Autowired
    private RestTemplate restTemplate;


    /**
     * Read - Get one card
     * @param id The id of the card
     * @return An Card object full filled
     */
    @GetMapping("/api/card/{id}")
    public CardDTO getById(@PathVariable final UUID id) {
        return cardService.getCard(id).map(this::convertToDto).orElse(null);
    }

    /**
     * Read - Get all cards
     * @return A list of Card object full filled
     */
    @GetMapping("/api/card")
    public List<CardDTO> getAll() {
        List<Card> cardList = (List<Card>) cardService.getAllCard();
        return cardList.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    /**
     * Read - Get cards by user
     * @return A list of Card object full filled
     */
    @GetMapping("/api/card/user/{id}")
    public List<CardDTO> getCardByUser(@PathVariable final UUID id) {
        List<Card> cardList = (List<Card>) cardService.getCardByUser(id);
        return cardList.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    /**
     * Create - Add a new card
     * @param cardDTO An object card
     * @return The card object saved
    */

    @PostMapping("/api/card")
    public CardDTO create(@RequestBody CardDTO cardDTO) throws ParseException {
        Card card = convertToEntity(cardDTO);
        Card cardCreate = cardService.createCard(card);
        return convertToDto(cardCreate);
    }

    /**
     * Create - Add a new card
     * @param cardDTO An object card
     * @param  id An id card
     * @return The card object saved
     */
    @PutMapping("/api/card/{id}")
    public CardDTO update(@RequestBody CardDTO cardDTO, @PathVariable final UUID id) throws ParseException {
        Card card = cardService.updateCard(id, convertToEntity(cardDTO));
        return convertToDto(card);
    }

    /**
     * Delete - Delete an card
     * @param id - The id of the card to delete
     */
    @DeleteMapping("/api/card/{id}")
    public void delete(@PathVariable final UUID id) {
        cardService.deleteCard(id);
    }

    private Card convertToEntity(CardDTO cardDto) throws ParseException {
        Card card = modelMapper.map(cardDto, Card.class);
        CardModelDTO cardModelDTO = cardModelService.getCardModel(UUID.fromString(cardDto.getCardModelStr())).map(this::convertToDto).orElse(null);
        CardModel cardModel = modelMapper.map(cardModelDTO, CardModel.class);
        card.setCardModel(cardModel);
        card.setOwnerUser(UUID.fromString(cardDto.getOwnerUserStr()));
        return card;
    }

    private CardDTO convertToDto(Card card) {
        CardDTO cardDTO = modelMapper.map(card, CardDTO.class);
        cardDTO.setCardModel(cardModelService.getCardModel(card.getCardModel().getId()).map(this::convertToDto).orElse(null));
        cardDTO.setOwnerUserStr(card.getOwnerUser().toString());
        UserDTO userDTO;
        try{
            userDTO = restTemplate.getForObject(UserAPI+card.getOwnerUser(), UserDTO.class);
        } catch (ClientErrorHandler.ResourceNotFoundException e) {
            userDTO = new UserDTO();
            userDTO.setId(card.getOwnerUser());
            e.printStackTrace();
        }
        cardDTO.setOwnerUser(userDTO);
        return cardDTO;
    }

    private CardModel convertToEntity(CardModelDTO cardModelDto) throws ParseException {
        CardModel cardModel = modelMapper.map(cardModelDto, CardModel.class);
        cardModel.setPrice(cardModelDto.getPrice());
        cardModel.setName(cardModelDto.getName());
        return cardModel;
    }

    private CardModelDTO convertToDto(CardModel cardModel) {
        CardModelDTO cardModelDTO = modelMapper.map(cardModel, CardModelDTO.class);
        cardModelDTO.setPrice(cardModel.getPrice());
        cardModelDTO.setName(cardModel.getName());
        return cardModelDTO;
    }

}

