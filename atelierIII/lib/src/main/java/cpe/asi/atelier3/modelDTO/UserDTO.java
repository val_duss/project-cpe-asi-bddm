package cpe.asi.atelier3.modelDTO;

import java.util.UUID;

public class UserDTO {

    private UUID id;
    private String name;
    private String password;
    private double balance;

    public UserDTO() {
    }

    public UserDTO(String name, String password, double balance) {
        this.name = name;
        this.password = password;
        this.balance = balance;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getBalance() {
        return balance;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", balance=" + balance +
                '}';
    }

}