package cpe.asi.atelier3.cardWS.model;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "cards")
public class Card {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(updatable = false, nullable = false)
    @ColumnDefault("random_uuid()")
    @Type(type = "uuid-char")
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "card_id", referencedColumnName = "id")
    private CardModel cardModel;
    private UUID ownerUser;


    public Card() {
    }

    public Card(CardModel cardModel, UUID ownerUser) {
        this.cardModel = cardModel;
        this.ownerUser = ownerUser;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Card{" +
                "id=" + id +
                ", cardModel=" + cardModel +
                ", ownerUser=" + ownerUser +
                '}';
    }

    public CardModel getCardModel() {
        return cardModel;
    }

    public void setCardModel(CardModel cardModel) {
        this.cardModel = cardModel;
    }

    public UUID getOwnerUser() {
        return ownerUser;
    }

    public void setOwnerUser(UUID ownerUser) {
        this.ownerUser = ownerUser;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Card)) return false;
        Card other = (Card) o;
        return this.getId() == other.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}