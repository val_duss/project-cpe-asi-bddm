import { get, post } from "./api_base.js";
import { Config } from "../config.js";

class ApiCards {
    static async getCards() {
        let reqURL = `${Config.baseApiUrl}/cards`;
        let response = await get(reqURL);
        console.log(response);
        return response;
    }
    static async createCard(data) {
        let reqURL = `${Config.baseApiUrl}/cards`;
        let response = await post(reqURL, data);
        console.log(response);
        return response;
    }
}

export { ApiCards };
