package cpe.asi.marketplace.dto;

import java.util.UUID;

public class CardDTO {

    private UUID id;
    private String cardModelStr;
    private String ownerUserStr;
    private CardModelDTO cardModel;
    private UserDTO ownerUser;

    public CardDTO() {
    }

    public CardDTO(String cardModel, String ownerUser) {
        this.cardModelStr = cardModel;
        this.ownerUserStr = ownerUser;
    }

    public CardDTO(CardModelDTO cardModelDTO, UserDTO ownerUserDTO) {
        this.cardModel = cardModelDTO;
        this.ownerUser = ownerUserDTO;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCardModelStr() {
        return cardModelStr;
    }

    public void setCardModelStr(String cardModelStr) {
        this.cardModelStr = cardModelStr;
    }

    public String getOwnerUserStr() {
        return ownerUserStr;
    }

    public void setOwnerUserStr(String ownerUserStr) {
        this.ownerUserStr = ownerUserStr;
    }

    public CardModelDTO getCardModel() {
        return cardModel;
    }

    public void setCardModel(CardModelDTO cardModel) {
        this.cardModel = cardModel;
    }

    public UserDTO getOwnerUser() {
        return ownerUser;
    }

    public void setOwnerUser(UserDTO ownerUser) {
        this.ownerUser = ownerUser;
    }
}