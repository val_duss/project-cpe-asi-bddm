package cpe.asi.marketplace.repositories;

import cpe.asi.marketplace.models.CardModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CardModelRepository extends CrudRepository<CardModel, UUID> {

}
