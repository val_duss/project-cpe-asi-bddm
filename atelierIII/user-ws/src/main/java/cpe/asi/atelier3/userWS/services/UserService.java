package cpe.asi.atelier3.userWS.services;

import java.util.Optional;
import java.util.UUID;

import cpe.asi.atelier3.userWS.models.User;
import cpe.asi.atelier3.userWS.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.codec.digest.DigestUtils;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public Iterable<User> getAllUsers(){
        return userRepository.findAll();
    }

    public Optional<User> getUser(final UUID id) {
        return userRepository.findById(id);
    }

    public void deleteUser(final UUID id) {
        userRepository.deleteById(id);
    }

    public User createUser(User user) {
        User savedUser = userRepository.save(user);
        return savedUser;
    }

    public User putUser(final UUID id, double balance) {
        return (userRepository.findById(id)
                .map(user -> {
                    user.setBalance(balance);
                    return userRepository.save(user);
                })
                .orElseGet(() -> {
                    return null;
                }));
    }

    public User checkLogin(String username, String password){
        User user = userRepository.findByUsername(username);
        if(user != null && password != ""){
            if(password.equals(user.getPassword())){
                return user;
            }
        }
        return null;
    }

}
