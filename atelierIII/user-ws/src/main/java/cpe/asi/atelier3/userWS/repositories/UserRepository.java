package cpe.asi.atelier3.userWS.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cpe.asi.atelier3.userWS.models.User;

import java.util.UUID;

@Repository
public interface UserRepository extends CrudRepository<User, UUID> {
    @Query("select u from User u where u.name = ?1")
    public User findByUsername(String username);
}
