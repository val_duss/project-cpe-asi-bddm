<h1>Atelier 3</h1>

<h2>Start</h2>

Pour le lancement du projet Atelier 3
il est requis d'avoir les librairies docker et docker-compose

```
./install.sh
```

les différents service sont sur les ports suivants:

-   front end: 8080
-   card-ws: 8081
-   user-ws: 8082
-   transaction-ws: 8083
-   room-ws: 8084

**Lien du repo gitlab : https://gitlab.com/val_duss/project-cpe-asi-bddm**

**Lien de la vidéo de démonstration : https://youtu.be/96tXRjGoHV8**

Nous avons un docker compose fonctionnel avec un reseau interne

Nous avons un problème avec la création de la bdd des rooms à cause du réseau interne docker.

La page d'accueil est Menu.html

Nous avons un problème d'authentification du à une erreur de transmission du JWT.

Le login est néanmoins foncitonnel, nous sommes redirigé vers le menu si l'authentification est validée, on reste sur login.html si non.

Sur le menu, nous avons 2 Options : SELL et BUY :

-   Sur SELL, les cartes récupérées sont celles de l'utilisateur courant . Le bouton de vente ne fonctionne pas dû au problème du JWT (nous n'avons pas l'id utilisateur). Il fonctionne néanmoins via la console

-   Sur Buy, les cartes affichées sont les transactions possédant l'id d'un "owner" et un "buyer" null. Si le buyer est non null, la transaction est déjà complète et donc non affichée. De même que pour les ventes de cartes, l'achat ne fonctionne pas via le front dû au JWT manquant. La transaction fonctionne quand même via la console.

Les rooms ont été développées mais ne sont pas intégrées au front.

Nous avons mis en place un test sur user afin de le tester, mais pas sur le reste.

En plus de cela, nous avons mis en place un swagger par WS pour faciliter le développement

<h2>Questions</h2>
<h3>Quelle est la différence entre un test fonctionnel et un test unitaire ? A quoi sert la couverture de code ?</h3>
Un test fonctionnel sert a vérifier le bon fonctionnement d'une fonction de l'application.

Un test unitaire vérifie lui le fonctionnement d'un bout de code l'application.

la couverture de code est une mesure utilisée pour décrire le taux de code source exécuté d'un programme

<h3>Qu’est ce qu’un test de non régression ? à quoi sert-t-il ?</h3>
C'est une procedure de test permettant d'assurer un non retour en arrière dans le developpement. Une regression est un developpement qui provoque une annomalie sur une autre fonction de l'application.
Le test permet d'éviter cela

<h3>Expliquer le principe de développement « test driven » ?</h3>
Le Test-Driven Development (TDD), ou développement piloté par les tests en français, est une méthode de développement de logiciel qui consiste à concevoir un logiciel par petites étapes, de façon progressive, en écrivant avant chaque partie du code source propre au logiciel les tests correspondants et en remaniant le code continuellement.

<h3>Quels intérêts présentent les micros services comparés aux architecture SOA ?</h3>
L'avantage des microservices est l'indépendance de chacun d'entre eux, de la scabilité de l'application et del'utilisation externe de ces derniers

<h3>Quelles sont les différences entre les micros services et le SOA ? Quel intérêt présente l’usage de
docker et des micro-services ?</h3>
SOA est de nature monolithique alors que les micro services sont de nature full stack. Les applications SOA sont concues pour effectuer de nombreuses taches, mais les micro services sont concus pour effectuer une seule tache. La SOA implique le partage du stockage de données entre les services tandis que dans les micro services, chaque service peut disposer d'un stockage de données indépendant. L'avantage des micros services c'es qu'ils peuvent fonctionner indépedamment.

<h3>Qu’est-ce que docker ? En quoi diffère-t-il des méthodes de virtualisation dites classiques (vmware,
virtualbox) ?</h3>
Docker est une plateforme permmettant de lancer certianes applciations dans des conteneurs logiciels. Contrairement à la virtualisation qui émule par logiciel différentes machines sur une machine physique. La conteneurisation émule différents OS sur un seul OS.

<h3>Quelle organisation en équipe permet la mise en œuvre de micro services ?</h3>
Les microservices favorisent l'organisation de petites équipes indépendantes, qui s'approprient leurs services. Les équipes agiseent au sein d'un contexte de petite taille et bien compris, et peuvent travailler de manière plus indépendante et rapide. CEla réduit la durée des cycles de dévéloppement

<h3>Que permet de faire l’outil Sonar ?</h3>
Sonar Qube sert à mesurer la qualité du code source en continu

<h3>Qu’est ce que l’intégration continue ? Quels avantages/contraintes présentent cette organisation ?</h3>
L'intégration continue est une méthode de développement de logiciel devops avec laquelle les développeurs intégrent régulièrement leurs modifications de code à un référentiel centralisé.
