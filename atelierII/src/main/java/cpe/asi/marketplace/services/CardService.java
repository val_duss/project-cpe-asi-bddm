package cpe.asi.marketplace.services;

import java.util.Optional;
import java.util.UUID;

import cpe.asi.marketplace.models.Card;
import cpe.asi.marketplace.models.User;
import cpe.asi.marketplace.repositories.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CardService {

    @Autowired
    private CardRepository cardRepository;

    public Iterable<Card> getAllCard(){
        return cardRepository.findAll();
    }

    public Optional<Card> getCard(final UUID id) {
        return cardRepository.findById(id);
    }

    public void deleteCard(final UUID id) {
        cardRepository.deleteById(id);
    }

    public Card createCard(Card card) {
        Card savedCard = cardRepository.save(card);
        return savedCard;
    }

    public Card putCard(final UUID id, User ownerUser) {
        return (cardRepository.findById(id)
                .map(card -> {
                    card.setOwnerUser(ownerUser);
                    return cardRepository.save(card);
                })
                .orElseGet(() -> {
                    return null;
                }));
    }
}
