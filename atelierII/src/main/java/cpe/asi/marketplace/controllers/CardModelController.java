package cpe.asi.marketplace.controllers;

import cpe.asi.marketplace.dto.CardModelDTO;
import cpe.asi.marketplace.models.CardModel;
import cpe.asi.marketplace.models.User;
import cpe.asi.marketplace.services.CardModelService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class CardModelController {

    @Autowired
    private CardModelService cardModelService;

    @Autowired
    private ModelMapper modelMapper;

    /**
     * Read - Get one cardmodel
     * @param id The id of the user
     * @return An CardModel object full filled
     */
    @GetMapping("/api/cardmodel/{id}")
    public CardModelDTO getById(@PathVariable final UUID id) {
        return convertToDto(cardModelService.getCardModel(id));
    }

    /**
     * Read - Get all cardmodels
     * @return A list of CardModel object full filled
     */
    @GetMapping("/api/cardmodel")
    public List<CardModelDTO> getAll() {
        List<CardModel> cardModelList = (List<CardModel>) cardModelService.getAllCardModel();
        return cardModelList.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    /**
     * Create - Add a new cardmodel
     * @param cardModel An object user
     * @return The cardmodel object saved
     */
    @PostMapping("/api/cardmodel")
    public CardModelDTO create(@RequestBody CardModelDTO cardModelDTO) throws ParseException {
        CardModel cardModel = convertToEntity(cardModelDTO);
        CardModel cardModelCreate = cardModelService.createCardModel(cardModel);
        return convertToDto(cardModelCreate);
    }

    /**
     * Delete - Delete an cardmodel
     * @param id - The id of the cardmodel to delete
     */
    @DeleteMapping("/api/cardmodel/{id}")
    public void delete(@PathVariable final UUID id) {
        cardModelService.deleteCardModel(id);
    }

    private CardModel convertToEntity(CardModelDTO cardModelDto) throws ParseException {
        CardModel cardModel = modelMapper.map(cardModelDto, CardModel.class);
        return cardModel;
    }

    private CardModelDTO convertToDto(Optional<CardModel> cardModel) {
        CardModelDTO cardModelDTO = modelMapper.map(cardModel, CardModelDTO.class);
        return cardModelDTO;
    }

    private CardModelDTO convertToDto(CardModel cardModel) {
        CardModelDTO cardModelDTO = modelMapper.map(cardModel, CardModelDTO.class);
        return cardModelDTO;
    }
}
