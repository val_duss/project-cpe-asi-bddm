function getBalance(){

    var tmp = httpGet("http://localhost:8082/api/users/connected");
    alert(tmp);

}

function httpGet(theUrl) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    var jwt = getCookie("JWT");
    let json="{\"JWT\":\""+ jwt +"\"}";

    xmlHttp.setRequestHeader("Accept", "application/json");
    xmlHttp.setRequestHeader("Content-Type", "application/json");

    xmlHttp.send( json );
    if( xmlHttp.status != 200){
       // alert(xmlHttp.responseText);
        //window.location.replace("login.html");
    }else {
        return xmlHttp.responseText;
    }
}


var user = {};
var titles = {};
function getCookie(name) {
    // Split cookie string and get all individual name=value pairs in an array
    var cookieArr = document.cookie.split(";");

    // Loop through the array elements
    for (var i = 0; i < cookieArr.length; i++) {
        var cookiePair = cookieArr[i].split("=");

        /* Removing whitespace at the beginning of the cookie name
        and compare it with the given string */
        if (name == cookiePair[0].trim()) {
            // Decode the cookie value and return
            return decodeURIComponent(cookiePair[1]);
        }
    }

    // Return null if not found
    return null;
}

function get_user() {
    $.ajax("/api/users/current",
        {
            dataType: 'json',
            success: function (data, status, xhr) {   // success callback function
                user = data;
                $("#money-user").html(user.balance);
                $("#name-user").html(user.username);
            },
            error: function (jqXhr, textStatus, errorMessage) { // error callback
                if (jqXhr.status === 515 || jqXhr.responseJSON.message.startsWith("errors.auth") || jqXhr.responseJSON.message.includes("JWT")) {
                    window.location.replace("login.html");
                }
            }
        }
    );
}

function get_titles(action = "buy") {
    let titles = {
        "tableName": 'Buy some cards !',
        "buttonTitle": 'Buy',
        "pageTitle": 'Buy cards',
        "action": 'buy',
        "pageDescription": 'Increase your collection !'
    };
    if (action === "sell") {
        titles = {
            "tableName": 'Sell your card(s)',
            "buttonTitle": 'Sell',
            "pageTitle": 'Sell cards',
            "action": 'sell',
            "pageDescription": 'Increase your money !'
        };
    }
    return titles;
}

function get_cards(action = "buy") {
    let url = "/api/cardsOnSale";
    if (user.id) {
        if (action === "sell") {
            url = "/api/cardsUser/" + user.id;
        }
    }
    $.ajax(url,
        {
            dataType: 'json',
            success: function (data, status, xhr) {   // success callback function
                display_cards(data);
            },
            error: function (jqXhr, textStatus, errorMessage) { // error callback
                alert(errorMessage);
            }
        }
    );
}

function display_cards(cards) {
    let template = document.querySelector("#row");
    let cardContainer = document.querySelector("#table-cards-content");

    $(cardContainer).children("tr").each(function () {
        $(this).remove();	// clean table
    });

    for (const card of cards) {
        let clone = document.importNode(template.content, true);
        newContent = clone.firstElementChild.innerHTML
            .replace(/{{family_src}}/g, card.family_src)
            .replace(/{{family_name}}/g, card.family_name)
            .replace(/{{name}}/g, card.name)
            .replace(/{{description}}/g, card.description)
            .replace(/{{hp}}/g, card.hp)
            .replace(/{{energy}}/g, card.energy)
            .replace(/{{attack}}/g, card.attack)
            .replace(/{{defense}}/g, card.defence)
            .replace(/{{price}}/g, card.price)
            .replace(/{{user}}/g, card.userId);
        clone.firstElementChild.innerHTML = newContent;
        $(clone.firstElementChild).attr("data-id", card.id);
        cardContainer.appendChild(clone);
    }

    $(".text-action").html(titles.buttonTitle);
    $(".action-button").each(function () {
        $(this).attr("data-action", titles.action);
    });
    $("#card-table").show(200);
}

function get_card_data(id) {
    $.ajax("/api/cards/" + id,
        {
            dataType: 'json',
            success: function (data, status, xhr) {   // success callback function
                set_data_card(data);
            },
            error: function (jqXhr, textStatus, errorMessage) { // error callback
                alert(errorMessage);
            }
        }
    );
}

function set_data_card(card) {
    $("#familyNameId").html(card.family_name);
    $(".cardHP").html(card.hp + " HP");
    $(".cardEnergy").html(card.energy);
    $("#cardNameImgId").html(card.name);
    $("#cardImgId").attr("src", card.img);
    $("#cardDescriptionId").html(card.description);
    $("#cardAttackId").html(card.attack + " Attack");
    $("#cardDefenceId").html("Defence " + card.defence);
    $("#cardPriceId").html(card.price + "$");
    $("#display-card").show(100);
}

function remove_line(id) {
    $("tr[data-id=" + id + "]").remove();
}

function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
