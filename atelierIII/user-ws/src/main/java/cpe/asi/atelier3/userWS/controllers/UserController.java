package cpe.asi.atelier3.userWS.controllers;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import cpe.asi.atelier3.auth.AuthService;
import cpe.asi.atelier3.modelDTO.UserDTO;
import cpe.asi.atelier3.userWS.models.User;
import cpe.asi.atelier3.userWS.services.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    private AuthService authService = new AuthService();


    /**
     * Read - Get one user
     * @param id The id of the user
     * @return An User object full filled
     */
    @GetMapping("/api/user/{id}")
    public UserDTO getById(@PathVariable final UUID id) {
        return userService.getUser(id).map(this::convertToDto).orElse(null);
    }

    /**
     * Read - Get all users
     * @return A list of User object full filled
     */
    @GetMapping("/api/user")
    public Iterable<UserDTO> getAll() {
        List<User> userList = (List<User>) userService.getAllUsers();
        return userList.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    /**
     * Create - Add a new user
     * @param userDTO An object user
     * @return The user object saved
     */
    @PostMapping("/api/user")
    public UserDTO create(@RequestBody UserDTO userDTO) throws ParseException {
        User user = convertToEntity(userDTO);
        User userCreate = userService.createUser(user);
        return convertToDto(userCreate);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/api/users/connected")
    public User getConnectedUser(@CookieValue(value = "JWT") String jwt){
        String id = authService.getId(jwt);

        User user = userService.getUser(UUID.fromString(id)).get();
        return user;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/api/auth/login")
    public UserDTO login(@RequestBody UserDTO userDTO, HttpServletResponse response) throws ParseException {
        User tempUser = convertToEntity(userDTO);
        User user = userService.checkLogin(tempUser.getName(), tempUser.getPassword());
        if (user == null){
            return null;
        }
        String jwt = Jwts.builder()
                .setIssuer("Aterlier3")
                .setIssuedAt(Date.from(Instant.now()))
                .setExpiration(Date.from(Instant.now().plus(6, ChronoUnit.HOURS)))
                .signWith(SignatureAlgorithm.HS256, AuthService.getSecret())
                .setSubject(user.getId().toString()).compact();

        Cookie cookie = new Cookie("JWT", jwt);
        cookie.setPath("/");

        response.addCookie(cookie);
        return convertToDto(user);
    }

    @GetMapping(value = "/api/auth")
    public void validate_token(@CookieValue(name = "JWT") String JWT, HttpServletResponse response){
        boolean is_jwt_correct = authService.checkJwt(JWT);

        if(!is_jwt_correct){
            Cookie cookie = new Cookie("JWT", null);
            cookie.setPath("/");
            response.addCookie(cookie);
        }
        response.setStatus(HttpStatus.NO_CONTENT.value());

    }


    /**
     * Put - Balance
     * @param id - The id of the user to update
     * @param balance - The new balance value
     */
    @PutMapping("/api/user/{id}")
    public UserDTO putBalance(@PathVariable final UUID id, @RequestBody double balance) {
        return convertToDto(userService.putUser(id, balance));
    }

    /**
     * Delete - Delete an user
     * @param id - The id of the user to delete
     */
    @DeleteMapping("/api/user/{id}")
    public void delete(@PathVariable final UUID id) {
        userService.deleteUser(id);
    }

    private User convertToEntity(UserDTO userDTO) throws ParseException {
        User user = modelMapper.map(userDTO, User.class);
        return user;
    }

    private UserDTO convertToDto(User user) {
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);
        return userDTO;
    }
}
