<h1>Atelier 3</h1>

<h2>Lien du git </h2>

->   https://gitlab.com/val_duss/project-cpe-asi-bddm.git

<h2>Membre du Groupe</h2>

- BERNARD Eloi
- DEGUEURCE Marco
- DUSSERVAIX Valentin
- MOOTE Aurélien

<h1>Session 1</h1>

<h2>Les éléments réalisés du cahier des charges</h2>

- BERNARD Eloi : Room Microservice et début du reverse proxy
- DEGUEURCE Marco : Static et Card Microservice
- DUSSERVAIX Valentin : Common lib et Transaction microservice (+début de tests unitaires)
- MOOTE Aurélien : Static et User Microservice

<h2>Les éléments non-réalisés du cahier des charges</h2>

Reverse proxy non terminé
Tests unitaires
Static non terminé
Vidéo Youtube
Couverture de tests (dont analyse Sonar)

<h1>Session 2</h1>

<h2>Les éléments réalisés</h2>

- Code complété
- Vidéo
- Nouveau service Game/Room
- Mise à jour de l'architecture
