package cpe.asi.atelier3.transactionWS.models;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "transactions")
public class Transaction {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(updatable = false, nullable = false)
    @ColumnDefault("random_uuid()")
    @Type(type = "uuid-char")
    private UUID id;
    private UUID card;
    private UUID buyerUser;
    private UUID sellerUser;
    private Date creationDate;
    private Date saleDate;


    public Transaction() {
        this.card = null;
        this.buyerUser = null;
        this.sellerUser = null;
    }

    public Transaction(UUID card, UUID sellerUser) {
        this.card = card;
        this.sellerUser = sellerUser;
        this.buyerUser = null;
        this.creationDate = new Date();
        this.saleDate = null;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getCard() {
        return card;
    }

    public void setCard(UUID card) {
        this.card = card;
    }

    public UUID getBuyerUser() {
        return buyerUser;
    }

    public void setBuyerUser(UUID buyerUser) {
        this.buyerUser = buyerUser;
    }

    public UUID getSellerUser() {
        return sellerUser;
    }

    public void setSellerUser(UUID sellerUser) {
        this.sellerUser = sellerUser;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Transaction)) return false;
        Transaction other = (Transaction) o;
        return this.getId() == other.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}