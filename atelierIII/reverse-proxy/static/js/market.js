function onload() {
    let cardList = [

    ]


    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", "http://localhost:8083/api/transaction", false ); // false for synchronous request
    xmlHttp.send( null );


    console.log(xmlHttp.status);

    var tab = JSON.parse(xmlHttp.responseText);
    alert(tab[0].id);
    for(var item in tab){
        if(tab[item].buyerUser==null) {
            cardList.push({
                family_name: "Marvel",
                img_src: "https://static.hitek.fr/img/actualite/2017/06/27/i_deadpool-2.jpg",
                name: tab[item].card.cardModel.name,
                description: "Une descri",
                hp: tab[item].card.cardModel.healthPoint,
                energy: 69,
                attack: tab[item].card.cardModel.attack,
                defense: tab[item].card.cardModel.defense,
                price: tab[item].card.cardModel.price,
                id_card: tab[item].card.id
            })
        }
    }





    let template = document.querySelector("#row");

    for(const card of cardList){

        let clone = document.importNode(template.content, true);

        newContent= clone.firstElementChild.innerHTML
            .replace(/{{family_src}}/g, card.family_src)
            .replace(/{{family_name}}/g, card.family_name)
            .replace(/{{img_src}}/g, card.img_src)
            .replace(/{{name}}/g, card.name)
            .replace(/{{description}}/g, card.description)
            .replace(/{{hp}}/g, card.hp)
            .replace(/{{energy}}/g, card.energy)
            .replace(/{{attack}}/g, card.attack)
            .replace(/{{defense}}/g, card.defense)
            .replace(/{{price}}/g, card.price);
        clone.firstElementChild.innerHTML= newContent;

        let cardContainer= document.querySelector("#tableContent");
        cardContainer.appendChild(clone);
    }



}






// "{\"sellerUserUUIDStr\": \""+idUser+"\",\"cardUUIDStr\": \""+idCard+"\"}";

function buy(idTransaction, idBuyer){
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "PUT", "http://localhost:8083/api/transaction/"+idTransaction, false ); // false for synchronous request


    let json = "{\"buyerUserUUIDStr\": \""+idBuyer+"\"}";
    xmlHttp.setRequestHeader("Accept", "application/json");
    xmlHttp.setRequestHeader("Content-Type", "application/json");

    xmlHttp.onload = () => console.log(xmlHttp.responseText);

    xmlHttp.send(json);
}
