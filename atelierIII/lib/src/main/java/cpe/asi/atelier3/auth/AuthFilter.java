package cpe.asi.atelier3.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

public class AuthFilter  extends OncePerRequestFilter {

    @Autowired
    AuthService authService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();
        String tokenJwt = null;

        if (cookies != null) {
            tokenJwt = Arrays.stream(cookies)
                    .filter(c -> "JWT".equals(c.getName()))
                    .findFirst()
                    .map(cookie -> cookie.getValue())
                    .orElse(null);
        }

        if (tokenJwt == null) {
            response.setContentType("application/json");
            response.setStatus(HttpStatus.FORBIDDEN.value());
            response.flushBuffer();
            response.getWriter().write(String.format("{\"message\": \"%s\"}", "errors.auth.no_jwt"));
            return;
        } else if (!authService.checkJwt(tokenJwt)) {
            response.setStatus(HttpStatus.FORBIDDEN.value());
            response.setContentType("application/json");
            response.flushBuffer();
            response.getWriter().write(String.format("{\"message\": \"%s\"}", "errors.auth.invalid_jwt"));
            return;
        }
        filterChain.doFilter(request, response);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        return true;
    }
}
