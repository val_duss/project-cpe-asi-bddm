package cpe.asi.marketplace.exceptions.users;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserError extends RuntimeException {

    public UserError(String message){
        super(message);
    }
}
