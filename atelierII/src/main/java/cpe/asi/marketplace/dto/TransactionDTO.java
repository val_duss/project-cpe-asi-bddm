package cpe.asi.marketplace.dto;

import cpe.asi.marketplace.models.Card;
import cpe.asi.marketplace.models.User;
import java.util.Date;
import java.util.UUID;

public class TransactionDTO {

    private UUID id;
    private CardDTO card;
    private UserDTO buyerUser;
    private UserDTO sellerUser;
    private String cardUUIDStr;
    private String sellerUserUUIDStr;
    private Date creationDate;
    private Date saleDate;

    public TransactionDTO() {
    }

    public TransactionDTO(String cardUUIDStr, String sellerUserUUIDStr) {
        this.cardUUIDStr = cardUUIDStr;
        this.sellerUserUUIDStr = sellerUserUUIDStr;
    }

    public TransactionDTO(CardDTO card, UserDTO sellerUser) {
        this.card = card;
        this.sellerUser = sellerUser;
        buyerUser = null;
        creationDate = new Date();
        saleDate = null;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public CardDTO getCard() {
        return card;
    }

    public void setCard(CardDTO card) {
        this.card = card;
    }

    public UserDTO getBuyerUser() {
        return buyerUser;
    }

    public void setBuyerUser(UserDTO buyerUser) {
        this.buyerUser = buyerUser;
    }

    public UserDTO getSellerUser() {
        return sellerUser;
    }

    public void setSellerUser(UserDTO sellerUser) {
        this.sellerUser = sellerUser;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public String getCardUUIDStr() {
        return cardUUIDStr;
    }

    public void setCardUUIDStr(String cardUUIDStr) {
        this.cardUUIDStr = cardUUIDStr;
    }

    public String getSellerUserUUIDStr() {
        return sellerUserUUIDStr;
    }

    public void setSellerUserUUIDStr(String sellerUserUUIDStr) {
        this.sellerUserUUIDStr = sellerUserUUIDStr;
    }
}