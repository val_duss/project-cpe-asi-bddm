const router = require("express").Router();

const {
    getRooms,
    getRoomById,
    createRoom,
    updateRoom,
    deleteRoom,
} = require("../../controllers/rooms/rooms_controller");

/**
 * @swagger
 * /rooms:
 *   get:
 *     produces:
 *       - application/json
 *     summary: Returns all rooms
 *     description: Returns all rooms
 *     responses:
 *       200:
 *         description: Returns all rooms
 */
router.get("/", (req, res) => {
    try {
        const roomsArray = getRooms(req, res);
        res.contentType("application/json").status(200).json(roomsArray);
    } catch (err) {

        res.sendStatus(500);
    }
});

/**
 * @swagger
 * /rooms/:id:
 *   get:
 *     produces:
 *       - application/json
 *     summary: Returns a Room by ID
 *     description: Returns a Room by ID
 *     responses:
 *       200:
 *         description: Returns specified Room
 *       404:
 *         description: Room not found
 */
router.get("/:id", (req, res) => {
    try {
        const room = getRoomById(req, res);
        res.contentType("application/json").status(200).json(room);
    } catch (err) {
        console.error('SALOPE')
        res.sendStatus(404);
    }
});

/**
 * @swagger
 * /rooms:
 *   post:
 *     produces:
 *       - application/json
 *     summary: Create a Room
 *     description: Create a Room
 *     responses:
 *       201:
 *         description: Returns successfully created Room
 */
router.post("/", (req, res) => {
    try {
        const room = createRoom(req, res);
        res.contentType("application/json").status(201).json(room);
    } catch (err) {
        res.sendStatus(500);
    }
});

/**
 * @swagger
 * /rooms/:id:
 *   put:
 *     produces:
 *       - application/json
 *     summary: Update a Room
 *     description: Update a Room
 *     responses:
 *       200:
 *         description: Returns successfully updated Room
 */
router.put("/:id", (req, res) => {
    try {
        const room = updateRoom(req, res);
        res.contentType("application/json").status(200).json(room);
    } catch (err) {
        res.sendStatus(500);
    }
});

/**
 * @swagger
 * /rooms/:id:
 *   delete:
 *     summary: Delete a Room
 *     description: Delete a Room
 *     responses:
 *       200:
 *         description: Deleted
 */
router.delete("/:id", (req, res) => {
    try {
        deleteRoom(req, res);
        res.contentType("application/json").sendStatus(200);
    } catch (err) {
        res.sendStatus(500);
    }
});

module.exports = router;
