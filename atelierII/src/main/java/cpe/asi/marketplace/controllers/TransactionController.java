package cpe.asi.marketplace.controllers;

import com.fasterxml.jackson.annotation.JsonProperty;
import cpe.asi.marketplace.dto.CardDTO;
import cpe.asi.marketplace.dto.CardModelDTO;
import cpe.asi.marketplace.dto.TransactionDTO;
import cpe.asi.marketplace.dto.UserDTO;
import cpe.asi.marketplace.models.Card;
import cpe.asi.marketplace.models.CardModel;
import cpe.asi.marketplace.models.Transaction;
import cpe.asi.marketplace.models.User;
import cpe.asi.marketplace.services.CardService;
import cpe.asi.marketplace.services.TransactionService;
import cpe.asi.marketplace.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private CardService cardService;

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    /**
     * Read - Get one Transaction
     * @param id The id of the Transaction
     * @return An Transaction object full filled
     */
    @GetMapping("/api/transaction/{id}")
    public TransactionDTO getById(@PathVariable final UUID id) { return convertToDto(transactionService.getTransaction(id));}

    /**
     * Read - Get all Transactions
     * @return A list of Transaction object full filled
     */
    @GetMapping("/api/transaction")
    public List<TransactionDTO> getAll() {
        List<Transaction> transactionList = (List<Transaction>) transactionService.getAllTransaction();
        return transactionList.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    /**
     * Create - Add a new Transaction
     * @param Transaction An object Transaction
     * @return The Transaction object saved
*/
    /**
    @PostMapping("/api/transaction")
    public Transaction create(@RequestBody Transaction Transaction) {
        return transactionService.createTransaction(Transaction);
    }*/
    @PostMapping("/api/transaction")
    public TransactionDTO create(@RequestBody TransactionDTO transactionDTO) throws ParseException {
        Transaction transaction = convertToEntity(transactionDTO);
        Transaction transactionCreate = transactionService.createTransaction(transaction);
        return convertToDto(transactionCreate);

    }


    /**
     * Delete - Delete an Transaction
     * @param id - The id of the Transaction to delete
     */
    @DeleteMapping("/api/transaction/{id}")
    public void delete(@PathVariable final UUID id) {
        transactionService.deleteTransaction(id);
    }

    @PutMapping("api/transaction/{id}")
    public Transaction putBuyer(@RequestBody RequestPutTransaction requestPutTransaction, @PathVariable final UUID id) {//TODO
        Optional<User> buyer   = userService.getUser(UUID.fromString(requestPutTransaction.getBuyerUUID()));
        Optional<Transaction> transaction = transactionService.getTransaction(id);

        if(buyer.isPresent() && transaction.isPresent()) {
            cardService.putCard(transaction.get().getCard().getId(), buyer.get());
            userService.putUser(buyer.get().getId(),buyer.get().getBalance() - transaction.get().getCard().getCardModel().getPrice());
            userService.putUser(transaction.get().getSellerUser().getId(),buyer.get().getBalance() + transaction.get().getCard().getCardModel().getPrice());

            return transactionService.putTransaction(id, buyer.get());
        } else {
            return null;
        }
    }

    private Transaction convertToEntity(TransactionDTO transactionDTO) throws ParseException {
        Transaction transaction = modelMapper.map(transactionDTO, Transaction.class);
        Optional<Card> card = cardService.getCard(UUID.fromString(transactionDTO.getCardUUIDStr()));
        Optional<User> seller = userService.getUser(UUID.fromString(transactionDTO.getSellerUserUUIDStr()));
        if(seller.isPresent() && card.isPresent()) {
            transaction.setCard(card.get());
            transaction.setSellerUser(seller.get());
        } else {
            return null;
        }
        return transaction;
    }

    private TransactionDTO convertToDto(Optional<Transaction> transaction) {
        TransactionDTO transactionDTO = modelMapper.map(transaction, TransactionDTO.class);
        transactionDTO.setCard(convertToDto(transaction.get().getCard()));
        transactionDTO.setSellerUser(convertToDto(transaction.get().getSellerUser()));
        if(transaction.get().getBuyerUser() != null){
            transactionDTO.setBuyerUser(convertToDto(transaction.get().getBuyerUser()));
        }
        return transactionDTO;
    }

    private TransactionDTO convertToDto(Transaction transaction) {
        TransactionDTO transactionDTO = modelMapper.map(transaction, TransactionDTO.class);
        transactionDTO.setCard(convertToDto(transaction.getCard()));
        transactionDTO.setSellerUser(convertToDto(transaction.getSellerUser()));
        if(transaction.getBuyerUser() != null){
            transactionDTO.setBuyerUser(convertToDto(transaction.getBuyerUser()));
        }
        return transactionDTO;
    }



    private CardDTO convertToDto(Card card) {
        CardDTO cardDTO = modelMapper.map(card, CardDTO.class);
        cardDTO.setCardModel(convertToDto(card.getCardModel()));
        cardDTO.setOwnerUser(convertToDto(card.getOwnerUser()));
        return cardDTO;
    }

    private CardModelDTO convertToDto(CardModel cardModel) {
        CardModelDTO cardModelDTO = modelMapper.map(cardModel, CardModelDTO.class);
        return cardModelDTO;
    }

    private UserDTO convertToDto(User user) {
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);
        return userDTO;
    }
}
class RequestPutTransaction{
    private String buyerUUID;

    public RequestPutTransaction(@JsonProperty("buyererUUID") String buyerUUID) {
        this.buyerUUID = buyerUUID;
    }

    public String getBuyerUUID() {
        return buyerUUID;
    }

    public void setBuyerUUID(String buyerUUID) {
        this.buyerUUID = buyerUUID;
    }
}

