-- Migrations
-- This table will keep a list of migrations that have been run on this database.
--
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS public.migrations (
  id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  file TEXT,
  created_at TIMESTAMP DEFAULT NOW()
);
