package cpe.asi.atelier3.cardWS.service;

import java.util.Optional;
import java.util.UUID;

import cpe.asi.atelier3.cardWS.model.Card;
import cpe.asi.atelier3.cardWS.repositories.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CardService {

    @Autowired
    private CardRepository cardRepository;

    public Iterable<Card> getAllCard(){
        return cardRepository.findAll();
    }

    public Iterable<Card> getCardByUser(final UUID id){
        return cardRepository.findByUser(id);
    }

    public Optional<Card> getCard(final UUID id) {
        return cardRepository.findById(id);
    }

    public void deleteCard(final UUID id) {
        cardRepository.deleteById(id);
    }

    public Card createCard(Card card) {
        Card savedCard = cardRepository.save(card);
        return savedCard;
    }

    public Card updateCard(final UUID id, Card cardToUpdate) {
        return (cardRepository.findById(id)
                .map(card -> {
                    card.setOwnerUser(cardToUpdate.getOwnerUser());
                    return cardRepository.save(card);
                })
                .orElseGet(() -> {
                    return null;
                }));
    }
}
