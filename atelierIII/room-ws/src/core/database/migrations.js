const { getClient } = require("./db_connect");
const promisify = require("util").promisify;
// Importing File System module
const fs = require("fs");

__dirname = "./src/core/database/sql_migrations";

async function getOutstandingMigrations(migrations = []) {
    const files = await promisify(fs.readdir)(__dirname);
    const sql = await Promise.all(
        files
            .filter((file) => file.split(".")[1] === "sql")
            .filter((file) => !migrations.includes(file))
            .map(async (file) => ({
                file,
                query: await promisify(fs.readFile)(`${__dirname}/${file}`, {
                    encoding: "utf-8",
                }),
            }))
    );
    return sql;
}

/**
 * Run existing migrations on databse
 */
async function migrate() {
    const client = await getClient();

    // Check previous migrations
    let existingMigrations = [];
    try {
        let result = await client.query("SELECT * FROM migrations");
        existingMigrations = result.rows.map((r) => r.file);
        console.log(`migrations already done : ${existingMigrations}`);
    } catch {
        console.error("First migration");
    }

    // Get outstanding migrations
    const outstandingMigrations = await getOutstandingMigrations(
        existingMigrations
    );

    try {
        // Start transaction
        await client.query("BEGIN");

        // Run each migration sequentially in a transaction
        for (let migration of outstandingMigrations) {
            // Run the migration
            await client.query(migration.query);
            console.log(`runned one migration : ${migration.file}`);
            // Keep track of the migration
            await client.query("INSERT INTO migrations (file) VALUES ($1)", [
                migration.file,
            ]);
        }

        // All good, we can commit the transaction
        await client.query("COMMIT");
    } catch (err) {
        // Oops, something went wrong, rollback!
        console.error("a migration failed, see below :\n");
        console.error(err);
        await client.query("ROLLBACK");
    } finally {
        // Don't forget to release the client!
        client.release();
        console.log("\nmigrations completed !\n");
    }
}

module.exports = {
    migrate,
};
