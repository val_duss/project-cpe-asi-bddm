package cpe.asi.marketplace.models;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "cardmodels")
public class CardModel {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(updatable = false, nullable = false)
    @ColumnDefault("random_uuid()")
    @Type(type = "uuid-char")
    private UUID id;
    private String name;
    private int attack;
    private int healthPoint;
    private int defense;
    private double price;

    public CardModel() {
    }

    public CardModel(String name, double price) {
        this.name = name;
        this.price = price;
        this.attack = 0;
        this.healthPoint = 0;
        this.defense = 0;
    }

    public CardModel(String name, int attack, int healthPoint, int sheild, double price) {
        this.name = name;
        this.attack = attack;
        this.healthPoint = healthPoint;
        this.defense = sheild;
        this.price = price;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getHealthPoint() {
        return healthPoint;
    }

    public void setHealthPoint(int healthPoint) {
        this.healthPoint = healthPoint;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int sheild) {
        this.defense = sheild;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof CardModel)) return false;
        CardModel other = (CardModel) o;
        return this.getId() == other.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}