const { getClient } = require("../../core/database/db_connect");
const { Room } = require("../../core/models/room");

/**
 * renvoie toutes les rooms
 * @param {Express request} req
 * @param {Express response} res
 * @returns liste de tout les rooms
 */
const getRoomsQuery = async (_req, res) => {
    // array pour récupérer les rooms
    let roomsArray = [];
    let client;
    try {
        // récupérer les rooms
        client = await getClient();
        const response = await client.query("SELECT * FROM asi.t_room;");

        // transformer en objets
        for (const room of response.rows) {
            roomsArray.push(Room.fromJSON(room));
        }
        client.release();
    } catch (error) {
        // une erreur s'est produite
        try {
            client.release();
        } catch (e) { }
        console.error(error);
        res.sendStatus(500);
    }
    return roomsArray;
};

/**
 * renvoie la room spécifiée
 * @param {Express request} req
 * @param {Express response} res
 * @returns room spécifié
 */
const getRoomByIdQuery = async (req, _res) => {
    let client;
    try {
        // récupérer le caserne demandé
        const id = req.params.id;
        client = await getClient();
        const response = await client.query(
            `SELECT * FROM asi.t_room WHERE id = '${id}';`
        );
        let room = Room.fromJSON(response.rows[0]);
        return room
    } catch (error) {
        // caserne non trouvé
        try {
            client.release();
        } catch (e) { }
        console.error(error);
        throw new Error('room.not_found');
    }
};

/**
 * créé une room
 * @param {Express request} req
 * @param {Express response} res
 * @returns room créé
 */
const createRoomQuery = async (req, _res) => {
    // on parse et on vérifie le body
    let user1Id = String(req.body.user1Id);
    let user2Id = String(req.body.user2Id);
    let client;
    try {
        client = await getClient();
        // creation de la room
        const response = await client.query(
            `insert into asi.t_room (user_1_id, user_2_id)
            values (${user1Id}, ${user2Id},) returning id, user_1_id, user_2_id;`
        );
        // transformer en objet
        let room = Room.fromJSON(response.rows[0], caserne);
        client.release();
        // envoi de la réponse
        return room;
    } catch (error) {
        // une erreur s'est produite
        try {
            client.release();
        } catch (e) { }
        console.error(error);
        throw new Error('room.error_creation');
    }
};

/**
 * met à jour une room
 * @param {Express request} req
 * @param {Express response} res
 * @returns room mis à jour
 */
const updateRoomQuery = async (req, _res) => {
    // on parse et on vérifie le body
    let user1Id = String(req.body.user1Id);
    let user2Id = String(req.body.user2Id);
    let client;
    try {
        const id = req.params.id;
        // update du room
        client = await getClient();
        const response = await client.query(
            `UPDATE asi.t_room
            SET user_1_id = ${user1Id}, user_2_id = ${user2Id}
            WHERE id = '${id}' returning id, user_1_id, user_2_id;`
        );
        client.release();
        // transformer en objet
        let room = Room.fromJSON(response.rows[0], caserne, feu);
        // envoi de la réponse
        return room;
    } catch (error) {
        // une erreur s'est produite
        try {
            client.release();
        } catch (e) { }
        console.error(error);
        throw new Error('room.error_updating');
    }
};

/**
 * supprime une room
 * @param {Express request} req
 * @param {Express response} res
 */
const deleteRoomQuery = async (req, _res) => {
    let client;
    try {
        const id = req.params.id;
        client = await getClient();
        await client.query(`DELETE FROM asi.t_room WHERE id='${id}';`);
        client.release();

    } catch (error) {
        //  une erreur s'est produite
        try {
            client.release();
        } catch (e) { }
        console.error(error);
        throw new Error('room.error_deletion');
    }
};

module.exports = {
    getRoomsQuery,
    getRoomByIdQuery,
    createRoomQuery,
    updateRoomQuery,
    deleteRoomQuery,
};
