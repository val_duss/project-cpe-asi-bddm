package cpe.asi.atelier3.modelDTO;

import java.util.UUID;

public class CardDTO {

    private UUID id;
    private String cardModelStr;
    private String ownerUserStr;
    private CardModelDTO cardModel;
    private UserDTO ownerUser;

    public CardDTO() {
        cardModel = null;
        cardModelStr = null;
        ownerUser = null;
        ownerUserStr = null;
    }

    public CardDTO(String cardModelStr, String ownerUserStr) {
        this.cardModelStr = cardModelStr;
        this.ownerUserStr = ownerUserStr;
        this.ownerUser = new UserDTO();
        this.ownerUser.setId(UUID.fromString(ownerUserStr));
        this.cardModel = new CardModelDTO();
        this.cardModel.setId(UUID.fromString(cardModelStr));
    }

    public CardDTO(CardModelDTO cardModelDTO, UserDTO ownerUserDTO) {
        this.cardModel = cardModelDTO;
        this.ownerUser = ownerUserDTO;
        this.cardModelStr = cardModelDTO.getId().toString();
        this.ownerUserStr = ownerUserDTO.getId().toString();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCardModelStr() {
        return cardModelStr;
    }

    public String getOwnerUserStr() {
        return ownerUserStr;
    }

    public void setOwnerUserStr(String ownerUserStr) {
        this.ownerUserStr = ownerUserStr;
        this.ownerUser = new UserDTO();
        this.ownerUser.setId(UUID.fromString(ownerUserStr));
    }

    public CardModelDTO getCardModel() {
        return cardModel;
    }

    public void setCardModel(CardModelDTO cardModel) {
        this.cardModel = cardModel;
    }

    public UserDTO getOwnerUser() {
        return ownerUser;
    }

    public void setOwnerUser(UserDTO ownerUser) {
        this.ownerUser = ownerUser;
    }

    public void setCardModelStr(String cardModelStr) {
        this.cardModelStr = cardModelStr;
        if(cardModelStr != null){
            this.cardModel = new CardModelDTO();
            this.cardModel.setId(UUID.fromString(cardModelStr));
        }
    }
}