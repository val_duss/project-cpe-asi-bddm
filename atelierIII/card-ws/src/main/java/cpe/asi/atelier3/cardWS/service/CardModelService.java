package cpe.asi.atelier3.cardWS.service;

import cpe.asi.atelier3.cardWS.model.CardModel;
import cpe.asi.atelier3.cardWS.repositories.CardModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class CardModelService {

    @Autowired
    private CardModelRepository cardModelRepository;

    public Iterable<CardModel> getAllCardModel(){
        return cardModelRepository.findAll();
    }

    public Optional<CardModel> getCardModel(final UUID id) {
        return cardModelRepository.findById(id);
    }

    public void deleteCardModel(final UUID id) {
        cardModelRepository.deleteById(id);
    }

    public CardModel createCardModel(CardModel card) {
        CardModel savedCardModel = cardModelRepository.save(card);
        return savedCardModel;
    }
}
