const {
    getRoomsQuery,
    getRoomByIdQuery,
    createRoomQuery,
    updateRoomQuery,
    deleteRoomQuery,
} = require("../../repositories/rooms/rooms_repository");

/**
 * renvoie toutes les rooms
 * @param {Express request} req
 * @param {Express response} res
 * @returns liste de tout les rooms
 */
const getRooms = async (req, res) => {
    //  get users
    try {
        return getRoomsQuery(req, res);
    } catch (err) {
        throw err;
    }
};

/**
 * renvoie la room spécifiée
 * @param {Express request} req
 * @param {Express response} res
 * @returns room spécifié
 */
const getRoomById = async (req, res) => {
    try {
        return getRoomByIdQuery(req, res);
    } catch (err) {
        throw err;
    }
};

/**
 * créé une room
 * @param {Express request} req
 * @param {Express response} res
 * @returns room créé
 */
const createRoom = async (req, res) => {
    // on parse et on vérifie le body
    let user1Id = String(req.body.user1Id);
    if (user1Id != null) {
        // TODO: verifier existance par requête sur user ws
    }
    let user2Id = String(req.body.user2Id);
    if (user2Id != null) {
        // TODO: verifier existance par requête sur user ws
    }
    try {
        return createRoomQuery(req, res);
    } catch (err) {
        throw err;
    }
};

/**
 * met à jour une room
 * @param {Express request} req
 * @param {Express response} res
 * @returns room mis à jour
 */
const updateRoom = async (req, res) => {
    let user1Id = String(req.body.user1Id);
    if (user1Id != null) {
        // TODO: verifier existance par requête sur user ws
    }
    let user2Id = String(req.body.user2Id);
    if (user2Id != null) {
        // TODO: verifier existance par requête sur user ws
    }
    try {
        return updateRoomQuery(req, res);
    } catch (err) {
        throw err;
    }
};

/**
 * supprime une room
 * @param {Express request} req
 * @param {Express response} res
 */
const deleteRoom = async (req, res) => {
    try {
        return deleteRoomQuery(req, res);
    } catch (err) {
        throw err;
    }
};

module.exports = {
    getRooms,
    getRoomById,
    createRoom,
    updateRoom,
    deleteRoom
}
