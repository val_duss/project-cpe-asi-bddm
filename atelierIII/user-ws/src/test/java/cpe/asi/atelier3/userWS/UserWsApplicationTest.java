package cpe.asi.atelier3.userWS;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class UserWsApplicationTest {

    @Test
    public void contextLoads() throws Exception {
        UserControllerTests userControllerTests = new UserControllerTests();
        userControllerTests.GetUser();
        userControllerTests.createUSer();
    }
}
