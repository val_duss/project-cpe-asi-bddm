package cpe.asi.marketplace.controllers;

import cpe.asi.marketplace.dto.CardDTO;
import cpe.asi.marketplace.dto.CardModelDTO;
import cpe.asi.marketplace.dto.UserDTO;
import cpe.asi.marketplace.models.Card;
import cpe.asi.marketplace.models.CardModel;
import cpe.asi.marketplace.models.User;
import cpe.asi.marketplace.services.CardModelService;
import cpe.asi.marketplace.services.CardService;
import cpe.asi.marketplace.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class CardController {

    @Autowired
    private CardService cardService;

    @Autowired
    private CardModelService cardModelService;

    @Autowired
    private UserService userService ;

    @Autowired
    private ModelMapper modelMapper;

    /**
     * Read - Get one card
     * @param id The id of the card
     * @return An Card object full filled
     */
    @GetMapping("/api/card/{id}")
    public CardDTO getById(@PathVariable final UUID id) {
        return convertToDto(cardService.getCard(id));
    }

    /**
     * Read - Get all cards
     * @return A list of Card object full filled
     */
    @GetMapping("/api/card")
    public List<CardDTO> getAll() {
        List<Card> cardList = (List<Card>) cardService.getAllCard();
        return cardList.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    /**
     * Create - Add a new card
     * @param card An object card
     * @return The card object saved
     */
    /**@PostMapping("/api/card")
    public Card create(@RequestBody Card card) {
        return cardService.createCard(card);
    }**/

    @PostMapping("/api/card")
    public CardDTO create(@RequestBody CardDTO cardDTO) throws ParseException {
        Card card = convertToEntity(cardDTO);
        Card cardCreate = cardService.createCard(card);
        return convertToDto(cardCreate);
    }

    /**
     * Delete - Delete an card
     * @param id - The id of the card to delete
     */
    @DeleteMapping("/api/card/{id}")
    public void delete(@PathVariable final UUID id) {
        cardService.deleteCard(id);
    }

    private Card convertToEntity(CardDTO cardDto) throws ParseException {
        Card card = modelMapper.map(cardDto, Card.class);
        Optional<CardModel> cardModel = cardModelService.getCardModel(UUID.fromString(cardDto.getCardModelStr()));
        Optional<User> user   = userService.getUser(UUID.fromString(cardDto.getOwnerUserStr()));
        if(user.isPresent() && cardModel.isPresent()) {
            card.setOwnerUser(user.get());
            card.setCardModel(cardModel.get());
        } else {
            return null;
        }
        return card;
    }

    private CardDTO convertToDto(Optional<Card> card) {
        CardDTO cardDTO = modelMapper.map(card, CardDTO.class);
        cardDTO.setCardModel(convertToDto(card.get().getCardModel()));
        cardDTO.setOwnerUser(convertToDto(card.get().getOwnerUser()));
        return cardDTO;
    }

    private CardDTO convertToDto(Card card) {
        CardDTO cardDTO = modelMapper.map(card, CardDTO.class);
        cardDTO.setCardModel(convertToDto(card.getCardModel()));
        cardDTO.setOwnerUser(convertToDto(card.getOwnerUser()));
        return cardDTO;
    }

    private CardModelDTO convertToDto(CardModel cardModel) {
        CardModelDTO cardModelDTO = modelMapper.map(cardModel, CardModelDTO.class);
        return cardModelDTO;
    }

    private UserDTO convertToDto(User user) {
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);
        return userDTO;
    }
}

