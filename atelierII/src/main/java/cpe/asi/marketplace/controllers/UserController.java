package cpe.asi.marketplace.controllers;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import cpe.asi.marketplace.dto.UserDTO;
import cpe.asi.marketplace.models.User;
import cpe.asi.marketplace.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import cpe.asi.marketplace.exceptions.users.LoginError;
import cpe.asi.marketplace.exceptions.users.UserError;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @RequestMapping(value = "api/user/login/{login}", method = RequestMethod.GET)
    public void login(@PathVariable String login, String password, HttpServletResponse response){
        // TODO: à implémenter
        if (login == null){
            throw new LoginError("errors.user.bad_login");
        }
        Algorithm algo = Algorithm.HMAC256("secret");
        String tokenJwt = JWT.create().withIssuer("monuser").sign(algo);
        response.addCookie(new Cookie("JWT", tokenJwt));
    }

    /**
     * Read - Get one user
     * @param id The id of the user
     * @return An User object full filled
     */
    @GetMapping("/api/user/{id}")
    public UserDTO getById(@PathVariable final UUID id) {
        Optional<User> user = userService.getUser(id);
        if(user.isPresent()) {
            return convertToDto(user.get());
        } else {
            throw new UserError("errors.user.not_found");
        }
    }

    /**
     * Read - Get all users
     * @return A list of User object full filled
     */
    @GetMapping("/api/user")
    public Iterable<UserDTO> getAll() {
        List<User> userList = (List<User>) userService.getAllUsers();
        return userList.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    /**
     * Create - Add a new user
     * @param userDTO An object user
     * @return The user object saved
     */
    @PostMapping("/api/user")
    public UserDTO create(@RequestBody UserDTO userDTO) throws ParseException {
        User user = convertToEntity(userDTO);
        User userCreate = userService.createUser(user);
        return convertToDto(userCreate);
    }

    /**
     * Delete - Delete an user
     * @param id - The id of the user to delete
     */
    @DeleteMapping("/api/user/{id}")
    public void delete(@PathVariable final UUID id) {
        userService.deleteUser(id);
    }

    private User convertToEntity(UserDTO userDTO) throws ParseException {
        User user = modelMapper.map(userDTO, User.class);
        return user;
    }

    private UserDTO convertToDto(Optional<User> user) {
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);
        return userDTO;
    }

    private UserDTO convertToDto(User user) {
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);
        return userDTO;
    }
}
