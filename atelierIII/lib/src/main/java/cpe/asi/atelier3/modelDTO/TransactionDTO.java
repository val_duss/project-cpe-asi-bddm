package cpe.asi.atelier3.modelDTO;

import java.util.Date;
import java.util.UUID;

public class TransactionDTO {

    private UUID id;
    private CardDTO card;
    private UserDTO buyerUser;
    private UserDTO sellerUser;
    private String cardUUIDStr;
    private String sellerUserUUIDStr;
    private String buyerUserUUIDStr;
    private Date creationDate;
    private Date saleDate;

    public TransactionDTO() {
        this.cardUUIDStr = null;
        this.sellerUserUUIDStr = null;
        this.buyerUserUUIDStr = null;
        this.card = null;
        this.sellerUser = null;
        this.buyerUser = null;
        this.creationDate = null;
        this.saleDate = null;
    }

    public TransactionDTO(String cardUUIDStr, String sellerUserUUIDStr) {
        this.cardUUIDStr = cardUUIDStr;
        this.sellerUserUUIDStr = sellerUserUUIDStr;
        this.buyerUserUUIDStr = null;
        this.card = new CardDTO();
        this.card.setId(UUID.fromString(cardUUIDStr));
        this.sellerUser = new UserDTO();
        this.sellerUser.setId(UUID.fromString(sellerUserUUIDStr));
    }

    public TransactionDTO(CardDTO card, UserDTO buyerUser) {
        this.card = card;
        this.buyerUser = buyerUser;
        this.sellerUser = null;
        this.cardUUIDStr = this.card.getId().toString();
        this.sellerUserUUIDStr = this.sellerUser.getId().toString();
        this.buyerUserUUIDStr = null;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public CardDTO getCard() {
        return card;
    }

    public void setCard(CardDTO card) {
        this.card = card;
        this.cardUUIDStr = card.getId().toString();
    }

    public UserDTO getBuyerUser() {
        return buyerUser;
    }

    public void setBuyerUser(UserDTO buyerUser) {
        this.buyerUser = buyerUser;
        this.buyerUserUUIDStr = buyerUser.getId().toString();
    }

    public UserDTO getSellerUser() {
        return sellerUser;
    }

    public void setSellerUser(UserDTO sellerUser) {
        this.sellerUser = sellerUser;
        this.sellerUserUUIDStr = sellerUser.getId().toString();
    }

    public String getCardUUIDStr() {
        return cardUUIDStr;
    }

    public void setCardUUIDStr(String cardUUIDStr) {
        this.cardUUIDStr = cardUUIDStr;
        this.card = new CardDTO();
        this.card.setId(UUID.fromString(cardUUIDStr));
    }

    public String getSellerUserUUIDStr() {
        return sellerUserUUIDStr;
    }

    public void setSellerUserUUIDStr(String sellerUserUUIDStr) {
        this.sellerUserUUIDStr = sellerUserUUIDStr;
        this.sellerUser = new UserDTO();
        this.sellerUser.setId(UUID.fromString(sellerUserUUIDStr));
    }

    public String getBuyerUserUUIDStr() {
        return buyerUserUUIDStr;
    }

    public void setBuyerUserUUIDStr(String buyerUserUUIDStr) {
        this.buyerUserUUIDStr = buyerUserUUIDStr;
        this.buyerUser = new UserDTO();
        this.buyerUser.setId(UUID.fromString(buyerUserUUIDStr));
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }
}