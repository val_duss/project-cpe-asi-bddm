package cpe.asi.marketplace.exceptions.users;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class LoginError extends RuntimeException {

    public LoginError(String message){
        super(message);
    }
}