package cpe.asi.atelier3.transactionWS.services;

import cpe.asi.atelier3.transactionWS.models.Transaction;
import cpe.asi.atelier3.transactionWS.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    public Iterable<Transaction> getAllTransaction(){ return transactionRepository.findAll(); }

    public Optional<Transaction> getTransaction(final UUID id) { return transactionRepository.findById(id); }

    public Transaction updateTransaction(final UUID id, Transaction transactionToUpdate) {
        return (transactionRepository.findById(id)
                .map(transaction -> {
                    if(transactionToUpdate.getBuyerUser() != null){
                        transaction.setBuyerUser(transactionToUpdate.getBuyerUser());
                    }
                    if(transactionToUpdate.getSellerUser() != null){
                        transaction.setSellerUser(transactionToUpdate.getSellerUser());
                    }
                    if(transactionToUpdate.getCard() != null){
                        transaction.setCard(transactionToUpdate.getCard());
                    }
                    return transactionRepository.save(transaction);
                })
                .orElseGet(() -> {
                    return null;
                }));
    }

    public void deleteTransaction(final UUID id) { transactionRepository.deleteById(id); }

    public Transaction createTransaction(Transaction card) {
        Transaction savedTransaction = transactionRepository.save(card);
        return savedTransaction;
    }
}
