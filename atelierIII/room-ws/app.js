#!/usr/bin/env node

const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express')
const app = express()

// import config
require('dotenv').config()
const port = process.env.RUNNING_PORT;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// require router
app.use('/', require('./src/core/router/router'));

app.listen(port, () => {
    console.log(`Serveur à l'écoute sur le port ${port}`)
})
