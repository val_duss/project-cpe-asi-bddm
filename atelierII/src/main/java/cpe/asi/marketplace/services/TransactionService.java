package cpe.asi.marketplace.services;

import cpe.asi.marketplace.models.Transaction;
import cpe.asi.marketplace.models.User;
import cpe.asi.marketplace.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    public Iterable<Transaction> getAllTransaction(){ return transactionRepository.findAll(); }

    public Optional<Transaction> getTransaction(final UUID id) { return transactionRepository.findById(id); }

    public Transaction putTransaction(final UUID id, User buyer) {
        return (transactionRepository.findById(id)
                .map(transaction -> {
                    transaction.setBuyerUser(buyer);
                    return transactionRepository.save(transaction);
                })
                .orElseGet(() -> {
                    return null;
                }));
    }

    public void deleteTransaction(final UUID id) { transactionRepository.deleteById(id); }

    public Transaction createTransaction(Transaction card) {
        Transaction savedTransaction = transactionRepository.save(card);
        return savedTransaction;
    }
}
